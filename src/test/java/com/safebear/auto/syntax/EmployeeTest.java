package com.safebear.auto.syntax;

import org.testng.annotations.Test;

public class EmployeeTest {@Test

    public void testEmployee(){

    //This is where we create our objects
    Employee hannah = new Employee();
    Employee bob = new Employee();
    SalesEmployee victoria = new SalesEmployee();

    //This is where we employ hannah and fire bob
    hannah.employ();
    bob.fire();

    //This is where we employ victoria and give her a bmw
    victoria.employ();
    victoria.changeCar("bmw");

    //let's print their state to screen
    System.out.println("Hannah employment state: " + hannah.isEmployed());
    System.out.println("Bob employment state: " + bob.isEmployed());
    System.out.println("Victoria's employment state: " + victoria.isEmployed());
    System.out.println("Victoria's car: " + victoria.car);
    System.out.println("Number of employees: " + "");
    System.out.println("Business turnover: " + "");
}
}
